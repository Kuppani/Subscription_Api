const router = require('express').Router();
const Subscribe = require('../../models/subscription');
const Methods = require('../../methods/custom');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');

const apiOperation = (req, res,crudMethod,optionsObj)=> {
	const bodyParams = Methods.initializer(req, Subscribe);
	crudMethod(bodyParams, optionsObj, (err, subscribe) => {
		console.log('\nsubscription details', subscribe);
		res.send(subscribe);
	})
}

// Here the roo path '/' is  '/api/plans/'
router.get('/', (req, res) => {
	res.send('This is the api route for plan management');
});

router.get('/getallSubscribes', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";
					Subscribe.selectTable(table);
					Subscribe.query('basket',(err,subscribes)=>{
						//The error is already handled in the query method; but the error first approach is used here, so if we wan't to do the error handling
						//here instead of the query function, it will be easier to switch back
						res.send(subscribes);
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/getSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+'_store_data';
					console.log("Table name"+table);
					Subscribe.selectTable(table);
					apiOperation(req, res, Subscribe.getItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

router.post('/addSubscribe', (req, res) => {
	const token = req.body.token || req.headers['token'];
	console.log("token:"+token);
	const bodyParams=req.body;
	console.log(bodyParams);
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
				const bodyParams = Methods.initializer(req, Subscribe);
				Subscribe.createItem(bodyParams, {
					table: decode.storeName+"_store_data",
					overwrite: false
				}, (err, subscribe) => {
					console.log('\nplan details', subscribe);
					res.send(subscribe);
				})
			}
        })
    }
    else{
        res.send("please send a token");
	}
});

router.put('/updateSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];
	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{

				const table = decode.storeName+"_store_data";
				Subscribe.selectTable(table);
				apiOperation(req, res, Subscribe.updateItem);
				   
            }
        })
    }
    else{
        res.send("please send a token");
    }
});

router.delete('/deleteSubscribe', (req, res) => {
	var token = req.body.token || req.headers['token'];	
		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = decode.storeName+"_store_data";
					Subscribe.selectTable(table);
					apiOperation(req, res, Subscribe.deleteItem);
				}
			})
		}
		else{
			res.send("please send a token");
		}
});

module.exports = router;